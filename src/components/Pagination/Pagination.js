import { useContext } from "react";
import { Context } from '../../store/StateContext';

const Pagination = () => {
    let { currentPage, setCurrentPage } = useContext(Context);

    const handlePagination = (sign) => {
        if (sign) {
          setCurrentPage(++currentPage)
        } else if (currentPage > 1) {
          setCurrentPage(--currentPage)
        }
      }

    return (
        <section className="pagination">
            <p onClick={() => handlePagination(false)}>-</p>
            <p>{currentPage}</p>
            <p onClick={() => handlePagination(true)}>+</p>
        </section>)
}

export default Pagination;