const Images = ({images}) => {
    return (<section className='images--wrapper'>

        {images.length !== 0 ? images.map(img => {
            return (
                <img className='img--a' key={`marsRov${img.id}`} src={img.img_src} alt={`marsRov${img.id}`} />)
        }) :
        <p>No pictures for this camera</p>
        }
    </section>)

}

export default Images;