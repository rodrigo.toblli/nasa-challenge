import { createContext, useState } from 'react'

export const Context = createContext()

const StateContext = ({ children }) => {
  const [loading, setLoading] = useState(true)
  const [pictures, setPictures] = useState([])
  let [camera, setCamera] = useState(false)
  let [currentPage, setCurrentPage] = useState(1)
  let [sol, setSol] = useState(1000)
  let [earth, setEarth] = useState(false)

  return (
    <Context.Provider
      value={{
        loading,
        setLoading,
        pictures,
        setPictures,
        camera,
        setCamera,
        currentPage,
        setCurrentPage,
        sol,
        setSol,
        earth,
        setEarth,
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default StateContext
