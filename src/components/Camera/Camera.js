import { useContext } from "react";
import { Context } from "../../store/StateContext";
import { cameraDictionary } from "../../camerasDB/cameraDB";

const Camera = () => {
  let { camera, setEarth, setCurrentPage, setCamera } = useContext(Context);
  const cameraClass = (key) => `cameras--items ${key === camera && 'selected'}`;
  return (
    <section className="menu--section">
      <h3>Select a camera</h3>
      {Object.entries(cameraDictionary).map(([key, value]) => {
        return (
          <p
            className={cameraClass(key)}
            key={value}
            onClick={() => {
              setCamera(key);
              setEarth(false);
              setCurrentPage(1);
            }}
          >
            {value}
          </p>
        );
      })}
      <p
        onClick={() => {
          setCamera(false);
          setCurrentPage(1);
        }}
      >
        X
      </p>
    </section>
  );
};

export default Camera;
