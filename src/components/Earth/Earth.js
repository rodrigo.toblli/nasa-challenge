import { useContext } from "react";
import { Context } from '../../store/StateContext';

const Earth = () => {
    let { earth, setEarth, setCurrentPage, setCamera, setSol } = useContext(Context);
    const classEarth = `${earth ? 'selected' : 'clickeable'}`;
    const EARTH_DATE = "2020-04-22";
    return (
        <section className='menu--section'>
            <p className={classEarth} onClick={() => { setEarth(EARTH_DATE); setCamera(false); setSol(1000); setCurrentPage(1) }}>Earth DAY</p>
            <p className="clickable" onClick={() => { setEarth(false); setCurrentPage(1) }}>X</p>
        </section>)
}

export default Earth;