import { render, screen } from '@testing-library/react';
import App from './App';
import StateContext from './store/StateContext';

test('App renders correctly', () => {
  render(
    <StateContext>
      <App />
    </StateContext>
  );
  const linkElement = screen.getByText(/wait/i);
  expect(linkElement).toBeInTheDocument();
});

