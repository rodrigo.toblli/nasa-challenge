import { useContext } from "react";
import { Context } from '../../store/StateContext';

const Sol = () => {
    let { setEarth, setCurrentPage, sol, setSol, setCamera } = useContext(Context);
    const classSol = `${sol === 2890 ? 'selected' : 'clickable'}`;
    return (
        <section className='menu--section'>
            <p className={classSol} onClick={() => { setSol(2890); setCamera(false); setEarth(false); setCurrentPage(1) }}>SOL DATE</p>
            <p className="clickable" onClick={() => { setSol(1000); setCurrentPage(1) }}>X</p>
        </section>)
}

export default Sol;