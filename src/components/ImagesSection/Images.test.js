import { render, screen } from '@testing-library/react';
import Images from './Images';
import StateContext from '../../store/StateContext';
import MockedImages from './Mock.js'

test('Images should have alternative test', () => {
  render(
      <StateContext>
        <Images images={MockedImages}/>
      </StateContext>
  );
  const labelElement = screen.getByAltText('marsRov102685');
  expect(labelElement).toBeInTheDocument();
});
