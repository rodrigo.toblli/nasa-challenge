import './App.css'
import axios from 'axios'
import { useContext, useEffect } from 'react'
import { Context } from './store/StateContext'
import Images from './components/ImagesSection/Images'
import Sol from './components/Sol/Sol'
import Earth from './components/Earth/Earth'
import Camera from './components/Camera/Camera'
import Pagination from './components/Pagination/Pagination'

function App() {
  const BASE_URL = 'https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos';
  const API_KEY = '3UQeHbbHOPkZsqDxD0w2djPYrSAOFIP5dhTW745P';

  let {
    loading,
    setLoading,
    sol,
    earth,
    camera,
    currentPage,
    pictures,
    setPictures,
  } = useContext(Context)

  useEffect(() => {
    setLoading(true)
    const urlCall =`${BASE_URL}?api_key=${API_KEY}&page=${currentPage}
    ${
      !earth
        ? `&sol=${sol}&${camera && `camera=${camera}`}`
        : `&earth_date=${earth}`
    }`;
    axios.get(urlCall)
      .then((res) => {
        setPictures(res.data.photos)
        setLoading(false)
      })
  }, [currentPage, camera, sol, earth, setLoading, setPictures])

  return (
    <main>
      <nav className="menu">
        <Camera />
        <Sol />
        <Earth />
      </nav>
      {loading ? (
        <p>wait</p>
      ) : (
        <>
          <Images images={pictures} />
          <Pagination />
        </>
      )}
    </main>
  )
}

export default App
