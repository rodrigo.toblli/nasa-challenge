import { render, screen, fireEvent } from '@testing-library/react';
import Camera from './Camera';
import StateContext from '../../store/StateContext';

test('Camera menu should hace "Select a camera" label', () => {
  render(
      <StateContext>
        <Camera />
      </StateContext>
  );
  const labelElement = screen.getByText(/Select a camera/i);
  expect(labelElement).toBeInTheDocument();
});

test('Click on certain camera should change class', () => {
    render(
        <StateContext>
          <Camera />
        </StateContext>
    );
    const cameraItem = screen.getByText(/Mast Camera/i);
    expect(cameraItem).toBeInTheDocument();
    fireEvent.click(cameraItem)
    expect(cameraItem.classList.contains('selected')).toBe(true);
  });